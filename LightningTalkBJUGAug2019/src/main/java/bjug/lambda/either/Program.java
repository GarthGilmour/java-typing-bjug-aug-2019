package bjug.lambda.either;

import com.jnape.palatable.lambda.adt.Either;

import java.time.DateTimeException;
import java.time.LocalDate;

import static java.time.format.DateTimeFormatter.ISO_DATE;

public class Program {
    public static void main(String[] args) {
        demoOnGoodDay();
        demoOnBadDay();
    }

    private static void demoOnGoodDay() {
        String result = buildDate(1)
                .match(Throwable::getMessage, ld -> ld.format(ISO_DATE));
        System.out.println(result);
    }

    private static void demoOnBadDay() {
        String result = buildDate(32)
                .match(Throwable::getMessage, ld -> ld.format(ISO_DATE));
        System.out.println(result);
    }

    private static Either<DateTimeException, LocalDate> buildDate(int dayOfMonth) {
        LocalDate today = LocalDate.now();
        try {
            return Either.right(today.withDayOfMonth(dayOfMonth));
        } catch(DateTimeException ex) {
            return Either.left(ex);
        }
    }
}
