package bjug.lambda.coproduct;

import com.jnape.palatable.lambda.adt.choice.Choice3;
import com.jnape.palatable.lambda.adt.coproduct.CoProduct3;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;

import static java.time.format.DateTimeFormatter.ISO_DATE;
import static java.time.format.DateTimeFormatter.ISO_TIME;

public class Program {
    public static void main(String[] args) {
        LocalDate rightNow = LocalDate.now();

        CoProduct3<LocalDate, LocalTime, Period, ?> result1 = demo(rightNow.getDayOfMonth());
        CoProduct3<LocalDate, LocalTime, Period, ?> result2 = demo(1);
        CoProduct3<LocalDate, LocalTime, Period, ?> result3 = demo(rightNow.lengthOfMonth());

        printResult(result1);
        printResult(result2);
        printResult(result3);
    }

    private static void printResult(CoProduct3<LocalDate, LocalTime, Period, ?> input) {
        String value = input.match(
                ld -> "Date of: " + ld.format(ISO_DATE),
                lt -> "Time of: " + lt.format(ISO_TIME),
                p -> "Period of: " + p
        );
        System.out.println(value);
    }

    private static CoProduct3<LocalDate, LocalTime, Period, ?> demo(int inputDay) {
        LocalDate today = LocalDate.now();
        LocalDate target = today.withDayOfMonth(inputDay);

        if(today.isEqual(target)) {
            return Choice3.b(LocalTime.now());
        } else if(today.isBefore(target)) {
            return Choice3.c(Period.between(today, target));
        } else {
            return Choice3.a(today);
        }
    }
}
