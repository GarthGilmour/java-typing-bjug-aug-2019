package bjug.lambda.currying;

import com.jnape.palatable.lambda.functions.Fn0;
import com.jnape.palatable.lambda.functions.Fn1;
import com.jnape.palatable.lambda.functions.Fn2;

import java.time.LocalDate;
import java.util.Scanner;

import static java.time.format.DateTimeFormatter.ISO_DATE;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Fn2<LocalDate, Integer, String> f1 = (ld, num) -> ld.plusDays(num).format(ISO_DATE);
        Fn1<Integer, String> f2 = f1.apply(LocalDate.now());
        Fn0<String> f3 = f2.thunk(3);

        System.out.println("Hit return when ready...");
        scanner.nextLine();
        System.out.println(f3.apply());
    }
}
