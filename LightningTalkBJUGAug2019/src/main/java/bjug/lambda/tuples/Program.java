package bjug.lambda.tuples;

import com.jnape.palatable.lambda.adt.hlist.HList;
import com.jnape.palatable.lambda.adt.hlist.Tuple2;

import java.time.LocalDate;

import static java.time.format.DateTimeFormatter.*;

public class Program {
    public static void main(String[] args) {
        Tuple2<LocalDate, LocalDate> result = demo();
        System.out.println(result._1());
        System.out.println(result._2());

        Tuple2<String, String> result2 = result.biMap(
          ld -> ld.format(ISO_ORDINAL_DATE),
          ld -> ld.format(ISO_WEEK_DATE)
        );
        System.out.println(result2._1());
        System.out.println(result2._2());
    }

    private static Tuple2<LocalDate, LocalDate> demo() {
        LocalDate firstOfThisMonth = LocalDate.now().withDayOfMonth(1);
        LocalDate firstOfNextMonth = LocalDate.of(
                firstOfThisMonth.getYear(),
                firstOfThisMonth.getMonth().plus(1),
                1);
        return HList.tuple(firstOfThisMonth, firstOfNextMonth);
    }
}
