package bjug.vavr.collections;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.List;

public class Program {
    private static List<Tuple2<String, Integer>> netflixShows() {
        return List.of(
                Tuple.of("Travellers", 5),
                Tuple.of("Another Life", 3),
                Tuple.of("Altered Carbon", 4),
                Tuple.of("Wu Assassins", 2),
                Tuple.of("Daredevil", 4),
                Tuple.of("Iron Fist", 1),
                Tuple.of("Star Trek Discovery", 5),
                Tuple.of("Titans", 3),
                Tuple.of("The Ministry of Time", 4));
    }

    public static void main(String[] args) {
        List<Tuple2<String, Integer>> shows = netflixShows();
        showStandardOperations(shows);
        showGrouping(shows);
        showCombinations(shows);
    }

    private static void showCombinations(List<Tuple2<String, Integer>> shows) {
        printTitle("Combinations");
        shows.filter(t -> t._2 == 4)
                .map(t -> t._1)
                .combinations(2)
                .map( items -> items.get(0) + " and " + items.get(1))
                .forEach(Program::printTabbed);
    }

    private static void showGrouping(List<Tuple2<String, Integer>> shows) {
        printTitle("Grouping");
        shows.groupBy(t -> t._2)
                .forEach((rating, items) -> {
                    printTabbed("Shows with a rating of " + rating);
                    items.map(t -> "\t" + t._1)
                            .forEach(Program::printTabbed);
                });
    }

    private static void showStandardOperations(List<Tuple2<String, Integer>> shows) {
        printTitle("Standard Operations");
        shows.filter(t -> t._2 == 5)
                .map(t -> t._1)
                .forEach(Program::printTabbed);
    }

    private static void printTitle(String title) {
        System.out.printf("--- %s ---\n", title);
    }

    private static void printTabbed(Object thing) {
        System.out.println("\t" + thing);
    }
}
