package bjug.vavr.matching;

import io.vavr.control.Option;
import io.vavr.control.Try;

import java.time.LocalDate;

import static io.vavr.API.*;
import static io.vavr.Patterns.*;
import static io.vavr.Predicates.isIn;
import static java.lang.String.format;

public class Program {

    private static int genNumber() {
        return (int)(Math.random() * 10);
    }

    private static Option<String> fetchProperty(String name) {
        return Option.of(System.getProperty(name));
    }

    private static Try<LocalDate> buildDate(int day) {
        return Try.of(() -> LocalDate.now().withDayOfMonth(day));
    }

    public static void main(String[] args) {
        showBasicMatching();
        showMatchingOnOption();
        showMatchingOnTry();
    }

    private static void showMatchingOnTry() {
        printTitle("Matching On Try");
        String result1 = Match(buildDate(1)).of(
                Case($Success($()), ld -> format("Date is '%s'", ld)),
                Case($Failure($()), ex -> format("Error is '%s'", ex.getMessage()))
        );
        String result2 = Match(buildDate(32)).of(
                Case($Success($()), ld -> format("Date is '%s'", ld)),
                Case($Failure($()), ex -> format("Error is '%s'", ex.getMessage()))
        );
        printTabbed(result1);
        printTabbed(result2);
    }

    private static void showMatchingOnOption() {
        printTitle("Matching On Option");
        String result1 = Match(fetchProperty("java.vendor")).of(
                Case($Some($()), str -> format("Vendor is '%s'", str)),
                Case($None(), "Cannot find vendor property")
        );
        String result2 = Match(fetchProperty("java.owner")).of(
                Case($Some($()), str -> format("Owner is '%s'", str)),
                Case($None(), "Cannot find owner property")
        );
        printTabbed(result1);
        printTabbed(result2);
    }

    private static void showBasicMatching() {
        printTitle("Basic Matching");
        String result = Match(genNumber()).of(
                Case($(0), "Zero"),
                Case($(1), "One"),
                Case($(isIn(2,3,4)), "First Range"),
                Case($(isIn(5,6,7)), val -> format("Second Range (%s)", val)),
                Case($(), val -> format("Something Else (%s)", val))
        );
        printTabbed(result);
    }

    private static void printTitle(String title) {
        System.out.printf("--- %s ---\n", title);
    }

    private static void printTabbed(Object thing) {
        System.out.println("\t" + thing);
    }
}
